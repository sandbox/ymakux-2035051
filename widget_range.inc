<?php

/**
 * @file
 *
 */

class FacetapiWidgetRange extends FacetapiWidget {

  /**
   * Renders the form.
   */
  public function execute() {

    $facet = $this->facet->getFacet();
    $adapter = $this->build['#adapter'];

    if ($facet) {
      $searched_values = $adapter->getActiveItems($facet);
      if (!empty($searched_values)) {
        $searched_values = reset($searched_values);
        $values = self::extractRange($searched_values['value']);

        if ((isset($values[0]) && isset($values[1])) && ($values[0] == $values[1])) {
          return FALSE;
        }
      }
    }

    $this->key = $this->build['#facet']['name'];
    $build = $this->facet->getBuild();
    $range = reset($build);
    $range_min = $range['#range_min'];
    $range_max = $range['#range_max'];
    if ($range_min == $range_max) {
      $this->build[$this->facet['field alias']] = NULL;
      return;
    }

    $this->build[$this->facet['field alias']] = drupal_get_form('facetapi_range_widget_form_' . $this->facet['field alias'], $adapter, $this->build);
  }

  /**
   * Form builder function for the facet block.
   */
  public static function widgetForm($form, $form_state, $form_id, FacetapiAdapter $adapter, $build = array()) {

    $settings = $build['#settings']->settings;
    $url_processor = $adapter->getUrlProcessor();
    $filter_key = $url_processor->getFilterKey();
    $field_name = isset($build['#facet']['field alias']) ? $build['#facet']['field alias'] : '';
    
    $range = array();
    if (isset($build['#facet']) && $field_name && isset($build[$field_name][$build['#facet']['field']])) {
      $range = $build[$field_name][$build['#facet']['field']];
    }

    $default_min = isset($range['#range_min']) ? $range['#range_min'] : 0;
    $default_max = isset($range['#range_max']) ? $range['#range_max'] : 0;

    if (!empty($_GET[$filter_key])) {
      foreach ($_GET[$filter_key] as $key => $filter) {
        $parts = explode(':', rawurldecode($filter), 2);
        if ((isset($parts[0]) && $parts[0] == $field_name) && isset($parts[1])) {
          $values = self::extractRange($parts[1]);
          if (isset($values[0])) {
            $default_min = ($values[0] == "*") ? '' : $values[0];
          }
          
          if (isset($values[1])) {
            $default_max = ($values[1] == "*") ? '' : $values[1];
          }
        }
      }
    }
    
    $convert = FALSE;
    switch ($settings['facetapi_range_action']) {
      case 'multiply' :
        $convert = 'divide';
      break;
      case 'divide' :
        $convert = 'multiply';
      break;
    }

    if ($convert && $factor = $settings['facetapi_range_factor']) {
      $default_min = self::prepareValue($default_min, $factor, $convert);
      $default_max = self::prepareValue($default_max, $factor, $convert);
    }

    $form['range']['min'] = array(
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $default_min,
      '#attributes' => array(
        'class' => array('min-range'),
      ),
    );
    $form['range']['max'] = array(
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $default_max,
      '#attributes' => array(
        'class' => array('max-range'),
      ),
    );
    $form['range']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('OK'),
    );
    $form['range']['settings'] = array(
      '#type' => 'value',
      '#value' => $settings,
    );
    $form['range']['field_alias'] = array(
      '#type' => 'value',
      '#value' => $field_name,
    );

    $form['slider']['#attached']['js'][] = drupal_get_path('module', 'facetapi_range') . '/facetapi_range.js';
    $form['#attributes']['id'] = 'facet_' . $field_name;
    return $form;
  }

  public static function widgetFormValidate($form, &$form_state) {

    $settings = $form_state['build_info']['args'][1]['#settings']->settings;
    $min = str_replace(' ', '', $form_state['values']['min']);
    $max = str_replace(' ', '', $form_state['values']['max']);
    
    if (!$min && !$max) {
      return drupal_goto($_GET['q']);
    }

    $min = $min ? str_replace(',', '.', $min) : 0;
    $max = $max ? str_replace(',', '.', $max) : 0;

    if (!is_numeric($min) && !is_numeric($max)) {
      return drupal_goto($_GET['q']);
    }

    $factor = $settings['facetapi_range_factor'];
    $action = $settings['facetapi_range_action'];

    $min = self::prepareValue($min, $factor, $action);
    $max = self::prepareValue($max, $factor, $action);
      
    if (!empty($form_state['build_info']['args'][1]['#facet']['map options']['index_type'])) {
      $index_type = $form_state['build_info']['args'][1]['#facet']['map options']['index_type'];
      if (substr($index_type, -3) == "int") {
        $form_state['values']['min'] = filter_var($min, FILTER_SANITIZE_NUMBER_INT);
        $form_state['values']['max'] = filter_var($max, FILTER_SANITIZE_NUMBER_INT);
      }
    }
  }

  public static function widgetFormSubmit($form, &$form_state) {
    $adapter = $form['#facetapi_adapter'];
    $url_processor = $adapter->getUrlProcessor();
    $filter_key = $url_processor->getFilterKey();

    $query_string = drupal_get_query_parameters();
    if (empty($query_string[$filter_key])) {
      $query_string[$filter_key] = array();
    }
    $my_field = rawurlencode($form_state['values']['field_alias']);

    // Remove any existing filters for my field.
    foreach ($query_string[$filter_key] as $idx => $filter) {
      $parts = explode(':', $filter, 2);
      if ($parts[0] == $my_field) {
        unset($query_string[$filter_key][$idx]);
      }
    }

    $min = $form_state['values']['min'] ? $form_state['values']['min'] : "*";
    $max = $form_state['values']['max'] ? $form_state['values']['max'] : "*";
    
    $query_string[$filter_key][] = $my_field . ':' . "[{$min} TO {$max}]";
    $form_state['redirect'] = array($_GET['q'], array('query' => $query_string));
  }

  public function settingsForm(&$form, &$form_state) {

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_action'] = array(
      '#title' => t('Action'),
      '#type' => 'select',
      '#options' => array(
        '<none>' => t('Do nothing'),
        'multiply' => t('Multiply by factor'),
        'divide' => t('Divide by factor'),
      ),
      '#default_value' => $this->settings->settings['facetapi_range_action'],
      '#description' => t('Choose how to render user entered value'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_factor'] = array(
      '#title' => t('Factor'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->settings->settings['facetapi_range_factor'],
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
        'invisible' => array(
          'select[name="facetapi_range_action"]' => array('value' => '<none>'),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_prefix_min'] = array(
      '#title' => t('Prefix Min'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->settings->settings['facetapi_range_prefix_min'],
      '#description' => t('Prefix for "Min" input field'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_suffix_min'] = array(
      '#title' => t('Suffix Min'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->settings->settings['facetapi_range_suffix_min'],
      '#description' => t('Suffix for "Min" input field'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_prefix_max'] = array(
      '#title' => t('Prefix Max'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->settings->settings['facetapi_range_prefix_max'],
      '#description' => t('Prefix for "Max" input field'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_suffix_max'] = array(
      '#title' => t('Suffix Max'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $this->settings->settings['facetapi_range_suffix_max'],
      '#description' => t('Suffix for "Max" input field'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $form['widget']['widget_settings']['facetapi_range']['facetapi_range_translate'] = array(
      '#title' => t('Translate suffix and prefix'),
      '#type' => 'checkbox',
      '#default_value' => $this->settings->settings['facetapi_range_translate'],
      '#description' => t('If selected, suffix and prefix you specified above can be translated via UI'),
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );
  }

  function getDefaultSettings() {
    return array(
      'facetapi_range_factor' => '',
      'facetapi_range_action' => '<none>',
      'facetapi_range_prefix_min' => '',
      'facetapi_range_suffix_min' => '',
      'facetapi_range_prefix_max' => '',
      'facetapi_range_suffix_max' => '',
      'facetapi_range_translate' => 0, 
    );
  }

  function prepareValue($value, $factor, $action) {
    $factor = trim($factor);
    if ($factor && is_numeric($factor) && is_numeric($value)) {
      switch ($action) {
        case 'multiply' :
          $value = $value * $factor;
        break;
        case 'divide' :
          $value = $value / $factor;
        break;
      }
    }
    return $value;
  }

  function extractRange($filter) {
    $values = str_replace('[', '', $filter);
    $values = str_replace(']', '', $values);
    return explode(' TO ', $values);
  }
}
