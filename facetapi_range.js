(function ($) {
  Drupal.behaviors.facetapi_range = {
    attach: function(context, settings) {
      for (var index in settings.facetapi.facets) {
        if (settings.facetapi.facets[index].widget == 'facetapi_range') {
          var formId = 'facet_' + settings.facetapi.facets[index].facetName;
          $('input[type=text]').keypress(function (e){
            if(e.which != 190 && e.which != 188 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)){
              return false;
            }
          });
          var submit = $('#' + formId + ' input[type=submit]');
          $('#' + formId + ' .min-range, #' + formId + ' .max-range').keyup(function() {
            var min = $('#' + formId + ' .min-range').val();
            var max = $('#' + formId + ' .max-range').val();
            if (min == "" && max == "") {
              submit.attr("disabled", "disabled");
            }
            else {
              submit.removeAttr("disabled");
            }
          });
        }
      }
    }
  }
})(jQuery);
